const {MaxHeap, MinHeap} = require('./heap');


const randN = max => Math.floor(Math.random() * Math.floor(max));

const testMin = () => {
  const minHeap = MinHeap();
  //const values = [9,1,2,8,7,6,5,4,3];
  const values = [1,2,3,4,5];
  for (let v of values) {
    minHeap.insert(v);
  }

  console.log('minHeap is', minHeap.isHeap());

  while (minHeap.data.length) {
    minHeap.pop();
  }
}

const testMax = () => {
  const maxHeap = MaxHeap();
  //const values = [9,1,2,8,7,6,5,4,3];
  const values = [1,2,3,4,5];
  for (let v of values) {
    maxHeap.insert(v);
  }

  console.log('maxHeap is', maxHeap.isHeap());
  while (maxHeap.data.length) {
    const i = Math.floor(maxHeap.data.length/2);
    maxHeap.remove(i);
  }
}

const testRandom = () => {
  global.silent = true;
  const N = 1000000
  const mh = MinHeap({notrace: true, nodot: true});
  const rs = Array.from({length:N}, () => randN(1000));
  const sorted = [...rs].sort((a,b) => a-b);
  rs.forEach(r => mh.insert(r));
  //ms = [];
  //while(mh.data.length) ms.push(mh.pop());
  console.log('Sort test', sorted.every(s => s === mh.pop()));
  //ms.forEach((m,i) => console.log(i, m, sorted[i]));
}


//testMin();
//testMax();
testRandom()
