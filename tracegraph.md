## insert: (55)
``` dot
digraph G1 {
  55 [style=filled, color=red];
55

}
```

## insert: (4)
``` dot
digraph G2 {
  55;
  4 [style=filled, color=red];
  55 -> 4;
}
```

## insert: (2)
``` dot
digraph G3 {
  55;
  4;
  2 [style=filled, color=red];
  55 -> 4;
  55 -> 2;
}
```

## insert: (51)
``` dot
digraph G4 {
  55;
  4;
  2;
  51 [style=filled, color=red];
  55 -> 4;
  55 -> 2;
  4 -> 51;
}
```

### Swap: 51 <-> 4
``` dot
digraph G5 {
  55;
  4 [style=filled, color=green];
  2;
  51 [style=filled, color=red];
  55 -> 4;
  55 -> 2;
  4 -> 51;
}
```

## insert: (95)
``` dot
digraph G6 {
  55;
  51;
  2;
  4;
  95 [style=filled, color=red];
  55 -> 51;
  55 -> 2;
  51 -> 4;
  51 -> 95;
}
```

### Swap: 95 <-> 51
``` dot
digraph G7 {
  55;
  51 [style=filled, color=green];
  2;
  4;
  95 [style=filled, color=red];
  55 -> 51;
  55 -> 2;
  51 -> 4;
  51 -> 95;
}
```

### Swap: 95 <-> 55
``` dot
digraph G8 {
  55 [style=filled, color=green];
  95 [style=filled, color=red];
  2;
  4;
  51;
  55 -> 95;
  55 -> 2;
  95 -> 4;
  95 -> 51;
}
```

