const fs = require('fs');

let graphId = 0;

// This [1] plugin will allow you see the graph.
// install and then open an md file and issue command
// :MarkdownPreview
//
// 1: https://github.com/iamcco/markdown-preview.nvim
//
const dot = (nodot = false) => (heap, header, c1, c2) => {
  if (nodot) return;

  const ordering = heap.data.reduce((acc, v, i) => {
    if (i === c1) {
      acc.push (`  ${v} [style=filled, color=red];`);
    } else if (i === c2) {
      acc.push (`  ${v} [style=filled, color=green];`);
    } else {
      acc.push (`  ${v};`);
    }
    return acc;
  }, []).join('\n') + '\n';

  const l = heap.data.length;

  const dag = l > 1
    ? heap.data.reduce((acc, p, i) => {
        let n = heap.leftChildIndex(i);
        if (n < l) { acc.push(`  ${p} -> ${heap.at(n)};`); }
        n = heap.rightChildIndex(i);
        if (n < l) { acc.push(`  ${p} -> ${heap.at(n)};`); }
        return acc;
      }, []).join('\n')
    : heap.data[0] + '\n';

  header += '\n';
  const fence = "```";
  const prolog = `digraph G${++graphId} {\n`;
  const epilog = "\n}\n";
  const uml = header + fence + " dot\n" + prolog + ordering + dag + epilog + fence + '\n\n';

  fs.writeFileSync("./tracegraph.md", uml, {flag: graphId === 1 ? 'w' : 'a'});
}


module.exports = {
  dot
}
