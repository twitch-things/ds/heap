# Experiment with heap DS

This was inspired by Dan's (Roxkstar74) recent [episode on
Heaps](https://www.twitch.tv/videos/739342950) which I watched with my partner who was surprisingly engaged.

This node project adds two visualizations to look at whats happening during the heap operations.
  - a tabular output that aligns header (index) and value, as well as cues to infer the nodes involved in a swap operation.
  - directed graph which is generated with
    [graphviz](https://graphviz.org/gallery/).  There's probably a way to
    integrate it with your editor of choice. I use neovim and the
    [markdown-preview.nvim
    plugin](https://github.com/iamcco/markdown-preview.nvim)

## Play with it
  - start a node repl in the root
    ```
    $ node

    ```
  - load the example repl.js, which will execute the commands in `repl.js` one
    at a time and leave you with a `min` and `max` heap. Some commands:
    `min.insert(someNumberValue)`, `min.pop()`, `min.remove(anIndex)`. The
    command line gives you access to the tabular trace.
    ```
    > .load repl.js
    > min.insert(99) # see what output looks like below

    ```




## Tabular output max-heap
```
λ ~/dev/twitch/heap/ node
> .load repl.js

inserting 55
    0
   55

inserting 4
    0     1
   55     4

inserting 2
    0     1     2
   55     4     2

inserting 51
    0     1     2     3
   55     4     2    51
   55  (51)     2   (4)

inserting 95
    0     1     2     3     4
   55    51     2     4    95
   55  (95)     2     4  (51)
 (95)  (55)     2     4    51
undefined
>

```
## Graph view
![directed graph](img/dot-example.png)

# TODO
- [x] first commit / insert operation
- [ ] refactor first commit
  - [x] Log
  - [x] isHeap
  - [x] dot - dump multiple trees
- [x] pop operation
- [x] remove a random node -> maintain heap invariants
- [x] refactor into a class implementation
- [ ] add tests
- [ ] give examples of heap applications
- [ ] benchmark maintaining a sorted array, vs a heap.
- [ ] how efficient is JavaScript's array implementation? good question
