const {dot} = require('./util');

const Log = msg => console.log(msg);
const Pad = (n, d) => `${' '.repeat(n)}${d}`.slice(-n);

class Heap {
  constructor(option) {
    this.data = [];
    this.cmp = option.cmp;
    this.extreme = option.extreme;
    if (option.notrace) this.trace = () => {};
    this.dot = dot(option.nodot);
  }

  leftChildIndex(i) { return 2 * i + 1; }
  rightChildIndex(i) { return 2 * i + 2; }
  parentIndex(i) { return Math.floor((i-1)/2); }

  at(i) { return i < this.data.length ? this.data[i] : this.extreme; }

  leftChildValue(i) { return this.at(this.leftChildIndex(i)); }
  rightChildValue(i) { return this.at(this.rightChildIndex(i)); }
  parentValue(i) { return this.at(this.parentIndex(i)); }

  isLocalHeap(i) {
    return this.cmp(this.at(i), this.leftChildValue(i)) &&
           this.cmp(this.at(i), this.rightChildValue(i));
  }

  isHeap() { return this.data.every((_, i) => this.isLocalHeap(i)) }

  swap(i, j) {
    this.dot(this, `### Swap: ${this.data[i]} <-> ${this.data[j]}`, i, j);
    const t = this.data[i];
    this.data[i] = this.data[j];
    this.data[j] = t;
    this.trace('', false, [i, j]);
  }

  heapUp(i) {
    const p = this.parentIndex(i);
    if(i && this.cmp(this.at(i), this.at(p))){
      this.swap(i, p);
      this.heapUp(p);
    }
  }

  heapDown(i) {
    if (i >= this.data.length) return;
    if (this.isLocalHeap(i)) return;

    const [l, r] = [this.leftChildIndex(i), this.rightChildIndex(i)];
    const extreme = this.cmp(this.at(l), this.at(r)) ? l : r;
    this.swap(i, extreme);
    this.heapDown(extreme);
  }

  insert(n) {
    this.data.push(n);
    this.trace(`\ninserting ${n}`);
    this.dot(this, `## insert: (${n})`, this.data.length-1);
    this.heapUp(this.data.length-1);
  }

  pop() {
    if (this.data.length === 0) return;

    this.dot(this, `## pop: (${this.data[0]})`, 0);
    this.swap(0, this.data.length-1, false);
    const root = this.data.pop();

    this.trace(`\nPop: ${root}`);
    if (this.data.length) this.heapDown(0);

    return root;
  }

  remove(i) {
    if (this.data.length === 0) return;
    if (i >= this.data.length) return;

    this.dot(this, `## remove: (${this.data[i]})`, i);

    this.swap(i, this.data.length-1, false);
    const root = this.data.pop();

    this.trace(`\nRemove(${i}): ${root}`);
    if (this.data.length) this.heapDown(i);

    return root;
  }

  trace(msg, showIndex=true, highlights=[]) {
    if (msg) Log(msg);
    let index = [];
    let values = [];

    this.data.forEach((v, i) => {
      index.push(Pad(5, i));
      const val = highlights.includes(i) ? '(' + v + ')' : v;
      values.push(Pad(5, val));
    });

    if (showIndex) Log(index.join(' '));
    Log(values.join(' '));
  }

};

const cmpMin = (a, b) => a < b;
const cmpMax = (a, b) => a > b;

const MaxHeap = (nodot = false, notrace = false) => {
    const h = new Heap({
      cmp: cmpMax,
      extreme: Number.NEGATIVE_INFINITY,
      nodot,
      notrace
    });
    return h;
  }

const MinHeap = (nodot = false, notrace = false) => {
    const h = new Heap({
      cmp: cmpMin,
      extreme: Number.POSITIVE_INFINITY,
      nodot,
      notrace
    });
    return h;
  }

module.exports = {
  MaxHeap, MinHeap
}
